from flask import Blueprint, url_for, render_template
from net_suite.model import RRType, BooleanSearchFilter, FqdnNameType, DBNetArea
from net_suite.search_results import SearchResult
from net_suite import db, db_conn, get_git_version, ModMetaData
import ipaddress

bb_name = 'dnsvs'
dnsvs = Blueprint(bb_name, __name__, template_folder="templates")


class RangeSerachResult(SearchResult):
    def __init__(self, range):
        self.range = range
        self.match_ref = url_for('dnsvs.inspect_area', area=range.name, suffix=range.suffix)

    def render(self):
        return render_template('dnsvs/search/range_search_result.html', range=self.range)


class RRSearchResult(SearchResult):
    def __init__(self, rr_data, fqdn, area, suffix, rr_type):
        self.fqdn = fqdn
        self.rr_data = rr_data
        self.match_ref = url_for('dnsvs.inspect_area', _anchor='sec' + rr_type, area=area, suffix=suffix)
        self.rr_type = rr_type

    def render(self):
        return render_template('dnsvs/search/rr_search_result.html', fqdn=self.fqdn, rr_data=self.rr_data,
                               rr_type=self.rr_type)


class FQDNRecsSearchResult(SearchResult):
    def __init__(self, fqdn, fqdn_description, rr_data, rr_type):
        self.fqdn = fqdn
        self.rr_data = rr_data
        self.fqdn_description = fqdn_description
        self.rr_type = rr_type
        self.match_ref = url_for('dnsvs.inspect_fqdn_recs', fqdn=fqdn)

    def render(self):
        return render_template('dnsvs/search/rr_search_result.html', fqdn=self.fqdn, rr_data=self.rr_data,
                               rr_type=self.rr_type)


class FQDNSearchResult(SearchResult):
    def __init__(self, fqdn, fqdn_description, parent_fqdn):
        self.fqdn = fqdn
        self.fqdn_description = fqdn_description
        self.match_ref = url_for('dnsvs.inspect_fqdn', fqdn=fqdn)

    def render(self):
        return render_template('dnsvs/search/fqdn_search_result.html', fqdn=self.fqdn,
                               fqdn_description=self.fqdn_description)


def search(db, connection, login, term, term_ipaddr, limit, opts=None):
    rr_types = [t[len('rr_type_'):] for t in opts if t.startswith('rr_type_')]
    fqdn_types = [t[len('s_fqdn_type_'):] for t in opts if t.startswith('s_fqdn_type')]
    res = list()
    if 'range_addrs' in opts and term_ipaddr is not None:
        s_res = db.execute(connection, DBNetArea.FULL_SELECT + """ 
        WHERE nd_ip_subnet.net >>= ({ip_term}::text::inet)
        """, {'ip_term': str(term_ipaddr)})
        res.extend((RangeSerachResult(DBNetArea(**r)) for r in s_res if
                    login.has_permission('dns.ro') or login.has_range(r['key_nr'])))
    if len(rr_types) > 0:
        s_res = list()
        if term_ipaddr is not None:
            s_res.extend(db.execute(connection, """
                select 
                  (select array_agg(name || suffix) from dns_range where key_nr = any(target_range_pk_list)) as target_ranges,
                  rr_data,
                  (rrt_rec).name as rrt_name,
                  fqdn,
                  fqdn_description
                 from 
                dns.list_rr(in_login_name:={login_name}, in_type_list:={type_list}, in_target_ipaddr_cidr_mask:={term_ipaddr}, in_listopt_global:={global}) order by fqdn limit {limit}
            """, {'term_ipaddr': str(term_ipaddr), 'login_name': login.login_name,
                  'global': login.has_permission('dns.ro'), 'limit': limit,
                  'type_list': rr_types}))
        s_res.extend(db.execute(connection, """
        with rr as (
                    (select
                      target_range_pk_list,
                      rr_data,
                      (rrt_rec).name as rrt_name,
                      fqdn,
                      fqdn_description
                     from
                    dns.list_rr(in_login_name:={login_name}, in_type_list:={type_list}, in_fqdn_regexp:={term}, in_listopt_global:={global}) limit {limit})
                    UNION
                    (select
                      target_range_pk_list,
                      rr_data,
                      (rrt_rec).name as rrt_name,
                      fqdn,
                      fqdn_description
                     from
                    dns.list_rr(in_login_name:={login_name}, in_type_list:={type_list}, in_target_fqdn_regexp:={term}, in_listopt_global:={global}) limit {limit})
)
select
  (select array_agg(name || suffix) from dns_range where key_nr = any(rr.target_range_pk_list)) as target_ranges,
  rr_data, rrt_name, fqdn, fqdn_description
from rr
order by fqdn
        """, {'term': term, 'login_name': login.login_name, 'global': login.has_permission('dns.ro'),
              'limit': limit,
              'type_list': rr_types}))
        for s in s_res:
            if s['target_ranges'] is None:
                res.append(
                    FQDNRecsSearchResult(fqdn=s['fqdn'], rr_type=s['rrt_name'], rr_data=s['rr_data'],
                                         fqdn_description=s['fqdn_description']))
                continue
            for a in s['target_ranges']:
                a = a.split('/')
                res.append(RRSearchResult(rr_type=s['rrt_name'], area=a[0], suffix=a[1], fqdn=s['fqdn'],
                                          rr_data=s['rr_data']))
    if term_ipaddr is None and len(fqdn_types) > 0:
        s_res = db.execute(connection, """
            select 
              fqdn,
              parent_fqdn,
              fqdn_description
             from 
            dns.list_fqdn(in_login_name:={login_name}, in_type_list:={inttypes} ,in_value_regexp:={term}, in_listopt_global:={global}) limit {limit}
        """, {'term': term, 'login_name': login.login_name, 'inttypes': fqdn_types,
              'global': login.has_permission('dns.ro'), 'limit': limit})
        for s in s_res:
            res.append(
                FQDNSearchResult(fqdn=s['fqdn'], fqdn_description=s['fqdn_description'], parent_fqdn=s['parent_fqdn']))

    return res


def gen_s_opts(db, connection, login):
    return {'Bereiche': [BooleanSearchFilter(name='Adressen in Bereich', form_name='range_addrs', default=True)],
            'Record-Typen': [
                BooleanSearchFilter(name=n.name, form_name='rr_type_{}'.format(n.name),
                                    default=n.name == 'A' or n.name == 'AAAA')
                for n in
                [r for r in RRType.get_all(db, connection) if
                 not r.is_intern]],
            'FQDN-Typen': [BooleanSearchFilter(name=n.description, form_name='s_fqdn_type_{}'.format(n.name),
                                               default=n.name == 'alias:0000' or n.name == 'dflt:0100' or n.name == 'dflt:1100')
                           for n in FqdnNameType.get_all(db, connection)]
            }


METADATA = ModMetaData(name=bb_name, mod_path=__name__, contact_email='dns-betrieb@scc.kit.edu',
                       gitlab_url='https://git.scc.kit.edu/scc-net/net-suite/net-suite-dnsvs',
                       printable_name='DNSVS', version=get_git_version(__file__), search_func=search,
                       search_opts_func=gen_s_opts)
MENU = {METADATA.printable_name: 'dnsvs.vs'}

from . import views
