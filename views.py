from flask import render_template, request, redirect, url_for, abort, jsonify, flash
from net_suite.model import *
from flask_breadcrumbs import register_breadcrumb
from . import dnsvs, METADATA
from net_suite import db, app
from net_suite.views import login_required, get_db_conn
from flask_wtf import FlaskForm
from wtforms import HiddenField, SelectField, StringField, FieldList, FormField, BooleanField
from wtforms.validators import DataRequired, Length, HostnameValidation
import itertools


@dnsvs.route('/')
@register_breadcrumb(dnsvs, '.', METADATA.printable_name)
@login_required
def vs():
    return redirect(url_for('dnsvs.entry_ranges'))


@dnsvs.route('/ranges')
@register_breadcrumb(dnsvs, '.ranges', 'Bereiche')
@login_required
def entry_ranges():
    s = request.environ['beaker.session']
    areas = s['login'].get_areas(db=db, connection=get_db_conn())
    counts = DBNetArea.get_area_utilization(db=db, connection=get_db_conn(), login=s['login'], areas=areas)
    return render_template('dnsvs/main_areas.html', areas=areas, counts=counts,
                           title=METADATA.printable_name)


@dnsvs.route('/fqdns', methods=['GET', 'POST'])
@register_breadcrumb(dnsvs, '.fqdns', 'FQDNs')
@login_required
def entry_fqdns():
    s = request.environ['beaker.session']
    if request.method == 'POST':
        del_f = FQDNDeleteForm()
        if del_f.validate_on_submit():
            s = request.environ['beaker.session']
            s['plan'].add_action(NTree.delete(fqdn=del_f.fqdn.data, do_del_pqdn=del_f.do_del_pqdn.data == 'True',
                                              force_del_ref_records=del_f.force_del_ref_records.data == 'True'))
            s.save()
        return redirect(url_for('dnsvs.entry_fqdns'))
    del_form = RRDeleteForm()
    fqdns = s['login'].get_fqdn_root_nodes(db=db, connection=get_db_conn())
    return render_template('dnsvs/main_fqdns.html', title="DNSVS", fqdns=fqdns, form=del_form)


def view_net(*args, **kwargs):
    if 'area' not in request.view_args:
        if 'fqdn' in request.view_args:
            return view_fqdn(args, kwargs)
        else:
            return list()
    return [{'text': request.view_args['area'] + '/' + request.view_args['suffix'],
             'url': url_for('dnsvs.inspect_area', area=request.view_args['area'],
                            suffix=request.view_args['suffix'])}]


def view_fqdn(*args, **kwargs):
    fqdn = request.view_args['fqdn']
    fqdn_path = fqdn.split('.')
    res = list()
    buf = ''
    for f in reversed(fqdn_path):
        if not f == '' and not f == '$':
            buf = f + '.' + buf
            rbuf = buf
        else:
            rbuf = '$'
        res.append({'text': (rbuf if not rbuf == '$' else '.'), 'url': url_for('dnsvs.inspect_fqdn', fqdn=rbuf)})
    return res


def view_net_rr(*args, **kwargs):
    if 'area' not in request.view_args:
        return [
            {'text': 'Record: ' + request.args['rr_data'],
             'url': url_for('dnsvs.edit_rr_data_fqdn', fqdn=request.view_args['fqdn'],
                            inttype=request.view_args['inttype'],
                            rr_data=request.args['rr_data'])}
        ]
    return [
        {'text': request.view_args['fqdn'],
         'url': url_for('dnsvs.edit_rr_data', area=request.view_args['area'], fqdn=request.view_args['fqdn'],
                        suffix=request.view_args['suffix'], inttype=request.view_args['inttype'],
                        rr_data=request.args['rr_data'])}
    ]


@dnsvs.route('/fqdns/<fqdn>', methods=['GET', 'POST'])
@register_breadcrumb(dnsvs, '.fqdns.fqdn', 'Referenzen', dynamic_list_constructor=view_fqdn)
@login_required
def inspect_fqdn(fqdn):
    s = request.environ['beaker.session']
    if fqdn == '$':
        fqdn = '.'
    if request.method == 'POST':
        del_f = FQDNDeleteForm()
        if del_f.validate_on_submit():
            s = request.environ['beaker.session']
            s['plan'].add_action(NTree.delete(fqdn=del_f.fqdn.data, do_del_pqdn=del_f.do_del_pqdn.data == 'True',
                                              force_del_ref_records=del_f.force_del_ref_records.data == 'True'))
            s.save()
        return redirect(url_for('dnsvs.inspect_fqdn', fqdn=fqdn))
    fqdns = s['login'].get_fqdn_nodes(db=db, connection=get_db_conn(), root=fqdn)
    fqdn = s['login'].get_fqdn(db=db, connection=get_db_conn(), fqdn=fqdn)
    if fqdn is None:
        return abort(404)
    del_form = RRDeleteForm()
    return render_template('dnsvs/inspect_fqdns.html', title="FQDN '{}'".format(fqdn.fqdn),
                           fqdn=fqdn,
                           fqdns=fqdns, form=del_form)


@dnsvs.route('/fqdns/<fqdn>/refs')
@register_breadcrumb(dnsvs, '.fqdns.fqdn.refs', 'Referenzen')
@login_required
def inspect_fqdn_refs(fqdn):
    s = request.environ['beaker.session']['login']
    if fqdn == '$':
        fqdn = '.'
    rr = RR.get_by_target_fqdn(login_name=s.login_name, target_fqdn=fqdn, connection=get_db_conn(), db=db)
    rr_grouped = dict()
    # group by rr_type and display panels like in inspect_area
    group_key_fn = lambda r: r['rr'].inttype.dns_rr_type_name
    for key, value_iter in itertools.groupby(rr, key=group_key_fn):
        rr_grouped[key] = [v for v in value_iter]
    rr_del_form = RRDeleteForm()
    return render_template('dnsvs/inspect_fqdn_refs.html', rr_del_form=rr_del_form,
                           title='Referenzen auf FQDN \'{}\''.format(fqdn),
                           rr=rr_grouped)


@dnsvs.route('/fqdns/<fqdn>/recs')
@register_breadcrumb(dnsvs, '.fqdns.fqdn.recs', 'Records')
@login_required
def inspect_fqdn_recs(fqdn):
    s = request.environ['beaker.session']['login']
    if fqdn == '$':
        fqdn = '.'
    rr = RR.get_by_fqdn(login_name=s.login_name, fqdn=fqdn, connection=get_db_conn(), db=db)
    rr_del_form = RRDeleteForm()
    return render_template('dnsvs/inspect_fqdn_recs.html', title='Records zu FQDN \'{}\''.format(fqdn), rr=rr,
                           fqdn=fqdn, rr_del_form=rr_del_form)


@dnsvs.route('/ranges/<area>/<suffix>/eventlog', methods=['GET', 'POST'])
@register_breadcrumb(dnsvs, '.ranges.net.eventlog', 'Eventlog')
@login_required
def area_evlog(area, suffix):
    s = request.environ['beaker.session']
    area_db = DBNetArea.get_by_name(name=area, suffix=suffix, db=db, connection=get_db_conn())
    if area_db is None:
        return abort(404)
    if not (s['login'].has_permission('dns.ro') or s['login'] in area_db.get_mgrs(db, connection=get_db_conn())):
        return abort(403)
    top_n = int(request.args.get('top_n', 10))
    ev = area_db.get_event_log(db=db, connection=get_db_conn(), top_n=top_n,
                               bnd_entity_id=area_db.print_name)
    group_by_date = dict()
    for e in ev:
        if e.timestamp not in group_by_date:
            group_by_date[e.timestamp] = list()
        group_by_date[e.timestamp].append(e)
    return render_template('dnsvs/eventlog.html', top_n=top_n,
                           title="Eventlog für Bereich '{area}'".format(area=area_db.print_name), area=area_db,
                           evitems=reversed(sorted(group_by_date.items())))


@dnsvs.route('/ranges/<area>/<suffix>', methods=['GET', 'POST'])
@register_breadcrumb(dnsvs, '.ranges.net', '', dynamic_list_constructor=view_net)
@login_required
def inspect_area(area, suffix):
    s = request.environ['beaker.session']
    area_db = DBNetArea.get_by_name(name=area, suffix=suffix, db=db, connection=get_db_conn())
    if area_db is None:
        return abort(404)
    nat_range = area_db.get_nat_range(db=db, connection=get_db_conn())
    mgrs = area_db.get_mgrs(db=db, connection=get_db_conn())
    rrs = area_db.get_rr(db, get_db_conn(), s['login'].login_name)
    possible_inttypes = s['login'].get_possible_rr_inttypes(db=db, connection=get_db_conn())
    disp_rr_types = [r for r in RRType.get_all(db, get_db_conn()) if
                     not r.is_intern or len(
                         [res for res in rrs if res['rr'].inttype.dns_rr_type_name == r.name]) > 0]  # TODO: filter/gen
    for r in rrs:
        del_form = RRDeleteForm(inttype=r['rr'].inttype.name, rr_data=r['rr'].rr_data, fqdn=r['NTree'].fqdn)
        r['del_form'] = del_form

    if area_db.net.net.version == 4:  # TODO: s.o
        disp_rr_types = [d for d in disp_rr_types if d.name != "AAAA"]
    else:
        disp_rr_types = [d for d in disp_rr_types if d.name != "A"]

    reserved_addrs = area_db.get_reserved_addrs(db=db, connection=get_db_conn())
    possible_rr_names = [p.dns_rr_type_name for p in possible_inttypes]
    rrs_by_r_name = dict()
    for r in rrs:
        if r['rr'].inttype.dns_rr_type_name not in rrs_by_r_name:
            rrs_by_r_name[r['rr'].inttype.dns_rr_type_name] = list()
        rrs_by_r_name[r['rr'].inttype.dns_rr_type_name].append(r)
    gateways = area_db.get_gateways(db, get_db_conn())
    evlog = area_db.get_event_log(db=db, connection=get_db_conn(),
                                  bnd_entity_id=area_db.print_name)
    return render_template('dnsvs/inspect_area.html',
                           last_ev=evlog[0] if len(evlog) > 0 else None,
                           area=area_db, mgrs=mgrs,
                           ntree=area_db.get_n_trees(db, get_db_conn()), page_header_class="net-header",
                           title="Bereich '{}'".format(area_db.print_name),
                           subtitle='<code>' + str(area_db.net.net) + '</code>',
                           possible_rr_names=possible_rr_names,
                           rr_types=disp_rr_types,
                           rrs=rrs_by_r_name,
                           reserved_addrs=reserved_addrs, area_max=area_db.get_max_addr(db, get_db_conn()),
                           area_min=area_db.get_min_addr(db, get_db_conn()),
                           gateways=gateways,
                           nat_range=nat_range)


def generate_data_fields_from_inttype(inttype):
    parts = inttype.name.split(':')
    left = [l for l in parts[1].split(',') if l is not '']
    right = [r for r in parts[2].split(',') if r is not ''][:-1]
    return left, right


@dnsvs.route('/ranges/<area>/<suffix>/<fqdn>/<inttype>/<rr_data>/typeahead-api-helper')
@dnsvs.route('/ranges/<area>/<suffix>/new/typeahead-api-helper')
@login_required
def fqdn_typeahed_helper(area, suffix, fqdn=None, inttype=None, rr_data=None):
    area_n_trees = DBNetArea.get_by_name(name=area, suffix=suffix, db=db, connection=get_db_conn()).get_n_trees(db=db,
                                                                                                                connection=get_db_conn())
    return jsonify([n.fqdn for n in area_n_trees])


@dnsvs.route('/fqdns/add', methods=['GET', 'POST'])
@register_breadcrumb(dnsvs, '.fqdns.new', 'Neuen FQDN anlegen')
@login_required
def add_fqdn():
    s = request.environ['beaker.session']['login']
    types = s.get_possible_fqdn_types(db=db, connection=get_db_conn())
    fqdn_form = FQDNAddForm()
    fqdn_form.inttype.choices = [(p.name, p.description) for p in types]
    if request.method == 'POST':
        if fqdn_form.validate_on_submit():
            s = request.environ['beaker.session']
            s['plan'].add_action(NTree.create(fqdn=fqdn_form.fqdn.data, description=fqdn_form.description.data,
                                              type=fqdn_form.inttype.data))
            s.save()
            return redirect(url_for('dnsvs.entry_fqdns'))
    return render_template('dnsvs/edit_fqdn.html', fqdn_form=fqdn_form, return_url=url_for('dnsvs.entry_fqdns'))


@dnsvs.route('/fqdns/<fqdn>/edit', methods=['GET', 'POST'])
@register_breadcrumb(dnsvs, '.fqdns.edit', 'Bearbeiten', dynamic_list_constructor=view_fqdn)
@login_required
def edit_fqdn(fqdn):
    s = request.environ['beaker.session']['login']
    types = s.get_possible_fqdn_types(db=db, connection=get_db_conn())
    ntree = s.get_fqdn(db=db, connection=get_db_conn(), fqdn=fqdn)
    if ntree is None:
        return abort(403)
    fqdn_form = FQDNAddForm(fqdn=ntree.fqdn, description=ntree.description)
    fqdn_form.inttype.choices = [(p.name, p.description) for p in types]
    if request.method == 'POST':
        if fqdn_form.validate_on_submit():
            s = request.environ['beaker.session']
            s['plan'].add_action(ntree.update(new_description=fqdn_form.description.data,
                                              new_fqdn=fqdn_form.fqdn.data,
                                              new_inttype=fqdn_form.inttype.data))
            s.save()
            return redirect(request.args.get('src', url_for('dnsvs.entry_fqdns')))

    fqdn_form.inttype.data = ntree.inttype
    return render_template('dnsvs/edit_fqdn.html', fqdn_form=fqdn_form, return_url=url_for('dnsvs.entry_fqdns'),
                           edit=True, ntree=ntree)


@dnsvs.route('/fqdns/<fqdn>/record/new', methods=['GET', 'POST'], endpoint='create_rr_data_fqdn')
@dnsvs.route('/record/new', methods=['GET', 'POST'], endpoint='create_rr_data_fast')
@dnsvs.route('/ranges/<area>/<suffix>/new/<type>', methods=['GET', 'POST'])
@register_breadcrumb(dnsvs, '.ranges.net.new', 'Neuen Record anlegen')
@login_required
def create_rr_data(type=None, area=None, suffix=None, fqdn=None):  # TODO
    s = request.environ['beaker.session']['login']
    inputs = app.config.get('DBRT_INPUTS')[type] if type in app.config.get('DBRT_INPUTS') else app.config.get(
        'DEFAULT_DBRT_INPUTS')
    rr_form = RRAddForm(rr_data=[{} for d in inputs['r']])
    i = 0
    for inp in inputs['r']:
        rr_form.rr_data[i].label = inp['label']
        rr_form.rr_data[i].description = inp['placeholder']
        if not request.method == 'POST':
            rr_form.rr_data[i].data = None
        i += 1
    possible_inttypes = s.get_possible_rr_inttypes(db=db, connection=get_db_conn())
    rr_form.inttype.choices = [(p.name, p.description) for p in possible_inttypes]
    area_obj = DBNetArea.get_by_name(connection=get_db_conn(), db=db, name=area,
                                     suffix=suffix) if area is not None else None
    prefix = None
    if (type == 'A' or type == 'AAAA') and request.method == 'GET':
        i = 0
        if 'rr_data' in request.args:
            prefix = request.args['rr_data']
        else:
            max = str(area_obj.net.net.broadcast_address)
            min = str(area_obj.net.net.network_address)
            prefix = ''
            while min[i] == max[i]:
                prefix = prefix + min[i]
                i = i + 1
                if i >= len(min):
                    break
    ret = None
    if 'description' in request.args:
        rr_form.description.data = request.args['description']
    if area_obj is None:
        if 'src' in request.args:
            if request.args['src'] == 'recs':
                ret = url_for('dnsvs.inspect_fqdn_recs', fqdn=fqdn)
        else:
            ret = url_for('dnsvs.vs')
    else:
        ret = url_for('dnsvs.inspect_area', area=area, suffix=suffix,
                      _anchor='sec' + type)
    if request.method == 'POST':
        if rr_form.validate_on_submit():
            s = request.environ['beaker.session']
            ntree = s['login'].get_fqdn(db=db, connection=get_db_conn(), fqdn=rr_form.fqdn.data)
            create_action = RR.create(fqdn=rr_form.fqdn.data,
                                      rr_data=rr_form.rr_data[1].data if len(rr_form.rr_data) > 1 else
                                      rr_form.rr_data[0].data,
                                      rr_data_unref=rr_form.rr_data[0].data if len(rr_form.rr_data) > 1 else None,
                                      inttype=rr_form.inttype.data, description=rr_form.description.data,
                                      with_description=ntree is None)  # todo sepcial error here
            if ntree is not None:
                if ntree.description is not None:
                    if rr_form.description_override.data and not ntree.description == rr_form.description.data:
                        s['plan'].add_action(
                            NTree.update_fqdn_description_deferrable(old_description=ntree.description,
                                                                     new_description=rr_form.description.data,
                                                                     fqdn=rr_form.fqdn.data))
                    elif not rr_form.description_override.data and not rr_form.description.data == '':
                        rr_form.description.errors.append('DNS-FQDN-Info für diesen FQDN bereits gesetzt.')
                        return render_template('dnsvs/area_edit_rr.html', inputs=inputs,
                                               possible_inttypes=possible_inttypes,
                                               rr_form=rr_form, area=area_obj,
                                               was_sub=True, rr=None, rr_type=type,
                                               return_url=ret)

            s['plan'].add_action(create_action)
            s.save()
            if (type == 'A' or type == 'AAAA') and 'add_next' in request.form:
                try:
                    addr = area_obj.get_next_free_address_after(db=db, connection=get_db_conn(), addr=(
                        ipaddress.IPv4Address(rr_form.rr_data[0].data) if type == 'A' else ipaddress.IPv6Address(
                            rr_form.rr_data[0].data)))
                except ipaddress.AddressValueError:
                    addr = None
                if addr is not None:
                    return redirect(
                        url_for('dnsvs.create_rr_data', type=type, area=area, suffix=suffix, rr_data=str(addr)))
                else:
                    return redirect(
                        url_for('dnsvs.create_rr_data', type=type, area=area, suffix=suffix, rr_data=""))
            return redirect(ret)
    else:
        rr_form.fqdn.data = fqdn
    return render_template('dnsvs/area_edit_rr.html', prefix=prefix, inputs=inputs, rr_type=type, rr=None,
                           possible_inttypes=possible_inttypes, rr_form=rr_form, area=area_obj,
                           return_url=ret)


@dnsvs.route('/ranges/<area>/<suffix>/<fqdn>/<inttype>/edit', methods=['GET', 'POST'])
@dnsvs.route('/fqdns/<fqdn>/record/<inttype>/edit', methods=['GET', 'POST'], endpoint='edit_rr_data_fqdn')
@register_breadcrumb(dnsvs, '.ranges.net.edit', '', dynamic_list_constructor=view_net_rr)
@login_required
def edit_rr_data(fqdn, inttype, area=None, suffix=None):
    rr_data = request.args['rr_data']
    s = request.environ['beaker.session']
    type = inttype.split(',')[-1]
    inputs = app.config.get('DBRT_INPUTS')[type] if type in app.config.get('DBRT_INPUTS') else app.config.get(
        'DEFAULT_DBRT_INPUTS')
    rr_form = RRAddForm(rr_data=[{} for d in inputs['r']])
    i = 0
    area_obj = DBNetArea.get_by_name(connection=get_db_conn(), db=db, name=area,
                                     suffix=suffix) if area is not None else None
    ret = None
    if area_obj is None:
        if request.args['src'] == 'recs':
            ret = url_for('dnsvs.inspect_fqdn_recs', fqdn=fqdn)
        else:
            ret = url_for('dnsvs.inspect_fqdn_refs', fqdn=rr_data)
    else:
        ret = url_for('dnsvs.inspect_area', area=area, suffix=suffix, _anchor='sec' + type)
    rr = RR.get(fqdn=fqdn, inttype=inttype, rr_data=rr_data, connection=get_db_conn(),
                db=db, login_name=s['login'].login_name)
    if rr is None:
        if 'trans_ret' in request.args:
            return redirect(ret)
        return abort(404)
    rr_data_list = [rr['rr'].rr_data_unref, rr['rr'].rr_data_target] if rr['rr'].rr_data_unref else [
        rr['rr'].rr_data_target]  # FIXME! I'm ugly
    for inp in inputs['r']:
        rr_form.rr_data[i].label = inp['label']
        rr_form.rr_data[i].description = inp['placeholder']
        if request.method != 'POST':
            rr_form.rr_data[i].data = rr_data_list[i]
        i += 1
    if not request.method == 'POST':
        rr_form.fqdn.data = rr['NTree'].fqdn
        rr_form.description.data = rr['NTree'].description
        rr_form.inttype.data = rr['rr'].inttype.name
    possible_inttypes = s['login'].get_possible_rr_inttypes(db=db, connection=get_db_conn())
    rr_form.inttype.choices = [(p.name, p.description) for p in possible_inttypes]
    if request.method == 'POST':
        if rr_form.validate_on_submit():
            s['plan'].add_action(rr['rr'].update_rr(
                rr_data=str(rr_form.rr_data[1].data) if len(rr_form.rr_data) > 1 else str(
                    rr_form.rr_data[0].data),
                rr_data_unref=rr_form.rr_data[0].data if len(rr_form.rr_data) > 1 else '',
                inttype=rr_form.inttype.data,
                old_fqdn=fqdn, new_fqdn=fqdn))
            s.save()
            return redirect(ret)
    if rr is None:
        abort(404)
    return render_template('dnsvs/area_edit_rr.html', inputs=inputs, rr=rr, possible_inttypes=possible_inttypes,
                           rr_form=rr_form, area=area_obj,
                           return_url=ret)


@dnsvs.route('/fqdns/<fqdn>/delete_rr', endpoint='delete_rr_fqdn', methods=['POST'])
@dnsvs.route('/ranges/<area>/<suffix>/delete_rr', methods=['POST'])
@login_required
def delete_rr(area=None, suffix=None, fqdn=None):
    s = request.environ['beaker.session']
    del_form = RRDeleteForm(request.form)
    if del_form.validate_on_submit():
        s['plan'].add_action(
            RR.delete(fqdn=del_form.fqdn.data, inttype=del_form.inttype.data, rr_data=del_form.rr_data.data))
        s.save()
        if area is None:
            ret = None
            if request.form['src'] == 'recs':
                ret = url_for('dnsvs.inspect_fqdn_recs', fqdn=fqdn)
            else:
                ret = url_for('dnsvs.inspect_fqdn_refs', fqdn=fqdn)
            return redirect(ret)
        return redirect(url_for('dnsvs.inspect_area', suffix=suffix, area=area))
    else:
        pass  # TODO: correct handling


class FQDNAddForm(FlaskForm):
    inttype = SelectField('inttype', validators=[DataRequired()])
    fqdn = StringField('fqdn', validators=[DataRequired()])
    description = StringField('fqdn-descr')


class RRDeleteForm(FlaskForm):
    action = HiddenField('delete', validators=[Length(0)])
    inttype = HiddenField('inttype')
    rr_data = HiddenField('rr_data')
    fqdn = HiddenField('fqdn')


class FQDNDeleteForm(FlaskForm):
    action = HiddenField('delete', validators=[Length(0)])
    fqdn = HiddenField('fqdn')
    do_del_pqdn = HiddenField('do_del_pqdn')
    force_del_ref_records = HiddenField('force_del_ref_records')


class RRAddForm(FlaskForm):
    inttype = SelectField('inttype', validators=[DataRequired()])
    fqdn = StringField('fqdn')
    rr_data = FieldList(StringField(), min_entries=1)
    description = StringField('fqdn-descr')
    description_override = BooleanField()


class AreaDescriptionForm(FlaskForm):
    description = StringField('description')


class AreaDomainDelForm(FlaskForm):
    fqdn = HiddenField('fqdn')


class AreaDomainAddForm(FlaskForm):
    fqdn = StringField('fqdn')


@dnsvs.route('/ranges/<area>/<suffix>/description', methods=['get', 'post'])
@login_required
@register_breadcrumb(dnsvs, '.ranges.net.description', 'Bereichs-Info')
def edit_range_descr(area, suffix):
    db_area = DBNetArea.get_by_name(name=area, suffix=suffix, db=db, connection=get_db_conn())
    descr_form = AreaDescriptionForm(description=db_area.description)
    if descr_form.validate_on_submit():
        s = request.environ['beaker.session']
        s['plan'].add_action(db_area.update_description(description=descr_form.description.data))
        s.save()
        return redirect(url_for('dnsvs.inspect_area', area=area, suffix=suffix))
    return render_template('dnsvs/area_edit_descr.html', descr_form=descr_form, area=db_area,
                           title='{} für Bereich \'{}\''.format('Bereichs-Info',
                                                                db_area.name + str(suffix)))


@dnsvs.route('/ranges/<area>/<suffix>/domains', methods=['post', 'get'])
@register_breadcrumb(dnsvs, '.ranges.net.domains', 'Domains')
@login_required
def edit_range_domains(area, suffix):
    a = DBNetArea.get_by_name(db=db, connection=get_db_conn(), name=area, suffix=suffix)
    add_form = AreaDomainAddForm()
    del_form = AreaDomainDelForm()
    if request.method == 'POST':
        if 'del' in request.form:
            if del_form.validate_on_submit():
                s = request.environ['beaker.session']
                s['plan'].add_action(a.delete_domains([del_form.fqdn.data]))
                s.save()
        if 'add' in request.form:
            if add_form.validate_on_submit():
                s = request.environ['beaker.session']
                s['plan'].add_action(a.add_domains(add_form.fqdn.data.split(' ')))
                s.save()
    return render_template('dnsvs/area_domains.html', add_form=add_form, del_form=del_form,
                           title="Domains für Bereich '{}' bearbeiten".format(a.print_name),
                           domains=a.get_n_trees(db=db, connection=get_db_conn()), area=a)


@dnsvs.route('/ranges/<area>/<suffix>/mgrs')
@register_breadcrumb(dnsvs, '.ranges.net.mgrs', 'Betreuer')
@login_required
def edit_mgrs(area, suffix):
    user = request.environ['beaker.session']['login']
    if not (user.is_scc_itb or user.has_permission('dns.rw_range')):
        return abort(403)
    a = DBNetArea.get_by_name(db=db, connection=get_db_conn(), name=area, suffix=suffix)
    return render_template('dnsvs/area_mgrs.html',
                           title="Betreuer für Bereich '{}' bearbeiten".format(a.print_name),
                           mgrs=a.get_mgrs(db, get_db_conn()), area=a)


@dnsvs.route('/ranges/<area>/<suffix>/mgrs/del', methods=['POST'])
@login_required
def del_mgr(area, suffix):
    user = request.environ['beaker.session']['login']
    a = DBNetArea.get_by_name(db=db, connection=get_db_conn(), name=area, suffix=suffix)
    mgr = request.form['login_name']
    s = request.environ['beaker.session']
    s['plan'].add_action(a.remove_mgrs([mgr]))
    s.save()
    return redirect(url_for('dnsvs.edit_mgrs', area=area, suffix=suffix))


@dnsvs.route('/ranges/<area>/<suffix>/mgrs/add', methods=['POST'])
@login_required
def add_mgr(area, suffix):
    user = request.environ['beaker.session']['login']
    a = DBNetArea.get_by_name(db=db, connection=get_db_conn(), name=area, suffix=suffix)
    mgr = request.form['login_name']
    s = request.environ['beaker.session']
    s['plan'].add_action(a.add_mgrs(mgr.strip().split(' ')))
    s.save()
    return redirect(url_for('dnsvs.edit_mgrs', area=area, suffix=suffix))
